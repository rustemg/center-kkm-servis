QT       += core gui widgets webkitwidgets network sql

TARGET = YandexMapsDemo
TEMPLATE = app


SOURCES += main.cpp\
    utils.cpp

HEADERS  += \
            connection.h \
    utils.h

RESOURCES += \
    res.qrc
