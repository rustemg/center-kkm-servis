#include "utils.h"

QString Utils::readFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        return NULL;
    }

    QTextStream stream;
    stream.setDevice(&file);
    QString txt =  stream.readAll();
    file.close();

    return txt;
}

QTemporaryFile *Utils::createTemporaryFile(const QString &content,
                                           QObject *parent)
{
    QTemporaryFile *tempFile = new QTemporaryFile(parent);
    if(!tempFile->open()) {
        return NULL;
    }

    QTextStream stream;
    stream.setDevice(tempFile);
    stream << content;
    tempFile->close();

    return tempFile;
}

QString Utils::changeFileName(QString fullPath, const QString newName)
{
    int n = fullPath.lastIndexOf("/");
    fullPath.chop(fullPath.length() - n - 1);
    return fullPath + newName;
}

