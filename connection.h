#ifndef CONNECTION_H
#define CONNECTION_H

#define SQL_TABLE_NAME "places"

#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery> 

static bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(":memory:");
    if (!db.open()) {
        QMessageBox::critical(0, "Ошибка",
            "Невозможно подключиться к БД", QMessageBox::Cancel);
        return false;
    }

    QList<QStringList> data;
    data.append(QStringList() << "КГЭУ"
                << "55.79985435303504"
                << "49.10349509521484");

    data.append(QStringList() << "КНИТУ"
                << "55.79771262587678"
                << "49.11364404934232");

    data.append(QStringList() << "КФУ"
                << "55.787506185695364"
                << "49.11331628763372");

    data.append(QStringList() << "КГМУ"
                << "55.78676399999366"
                << "49.12285299999998");

    QString sqlCreateTable = "CREATE TABLE %1"
                            "(description TEXT,"
                            "longitude TEXT,"
                            "latitude TEXT)";

    QString sqlInsertIntoTable = "INSERT INTO %1 VALUES"
                                    "( '%2', '%3', '%4')";

    QSqlQuery query;
    query.exec(sqlCreateTable.arg(SQL_TABLE_NAME));
    foreach(QStringList item, data) {
        query.exec(sqlInsertIntoTable.arg(SQL_TABLE_NAME,
                                          item[0], item[1],item[2]));
    }

    return true;
}

#endif
