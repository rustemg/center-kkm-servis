#include <QApplication>
#include <QWebView>
#include <QSqlQuery>
#include "connection.h"
#include "utils.h"

#define LOCAL_JS_CSS 0

#define RESOURCE_PREFIX ":/data/"

QString getDataFromDB()
{
    QString element = "{pos:[%1, %2], name: '%3'}";
    QStringList elementList;

    QSqlQuery q;
    q.exec(QString("SELECT * FROM %1").arg(SQL_TABLE_NAME));
    while (q.next()) {
        QString longitude = q.value(1).toString();
        QString latitude = q.value(2).toString();
        QString desc = QString::fromLocal8Bit(q.value(0).toByteArray());

        elementList.append(element.arg(longitude, latitude, desc));
    }

    return elementList.join(",");
}

void deployLocalJsAndCss()
{
    QStringList depFileNames;
    depFileNames << "bootstrap.min.css"
                 << "jquery.min.js"
                 << "yandex-maps.js";

    QList<QTemporaryFile*> depFiles;

    foreach(QString dep, depFileNames) {
        QString s = Utils::readFile(RESOURCE_PREFIX + dep);
        QTemporaryFile *f = Utils::createTemporaryFile(s, qApp);
        depFiles.append(f);
        f->rename(Utils::changeFileName(f->fileName(), dep));
    }
}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    if (!createConnection()) {
        return 1;
    }
#if LOCAL_JS_CSS
    QString templateHtml = "index-local-js-css.html";
    deployLocalJsAndCss();
#else
    QString templateHtml = "index.html";
#endif

    QString data = getDataFromDB();
    QString html =  Utils::readFile(RESOURCE_PREFIX + templateHtml);
    QTemporaryFile *htmlFile = Utils::createTemporaryFile(html.arg(data), qApp);
    htmlFile->rename(htmlFile->fileName() + ".html");

    QWebView *view = new QWebView();
    view->setFixedSize(600, 400);
    view->setWindowTitle("Газизов Рустем +7 965 604 74 34");
    view->load(QUrl::fromLocalFile(htmlFile->fileName()));
    view->show();

    return app.exec();
}
