#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QTextStream>
#include <QFile>
#include <QTemporaryFile>


class Utils
{
public:
    static QString readFile(const QString &fileName);
    static QTemporaryFile* createTemporaryFile(const QString &content, QObject *parent);
    static QString changeFileName(QString fullPath, const QString newName);

private:
    Utils();
};

#endif // UTILS_H
